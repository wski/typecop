const TypeCop = require('../../');

describe("string():", function() {
  it("returns unmodified string for valid string type", function() {
    expect(new TypeCop('a normal string', 'string').strict()).toBe('a normal string');
  });

  it("returns a stringified integer if type is integer", function() {
    expect(new TypeCop(1234, 'string').strict()).toBe('1234');
  });

  it("returns a stringified boolean if type is boolean", function() {
    expect(new TypeCop(true, 'string').strict()).toBe('true');
  });

  it("returns a stringified array if type is array", function() {
    expect(new TypeCop([true], 'string').strict()).toBe('[true]');
  });

  it("returns a stringified object if type is object", function() {
    expect(new TypeCop({true: true}, 'string').strict()).toBe('{"true":true}');
  });

  it("safely creates a string if type is function", function() {
    expect(new TypeCop(function(){}, 'string').strict()).toBe('string');
  });
});
