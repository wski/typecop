const TypeCop = require('../../');

describe("array():", function() {
  it("returns unmodified array for valid array type with no subtype", function() {
    expect(new TypeCop([1,'frog',3], 'array').strict()).toEqual([1,'frog',3]);
  });
  it("returns unmodified array for valid array type", function() {
    expect(new TypeCop([1,2,3], 'array', 'integer').strict()).toEqual([1,2,3]);
  });
  it("removed bad values from array that don't match subtype (string)", function() {
    expect(new TypeCop([1,'dog',2,3], 'array', 'integer').strict()).toEqual([1,0,2,3]);
  });
  it("removed bad values from array that don't match subtype (function)", function() {
    expect(new TypeCop([1,function(){},2,3], 'array', 'integer').strict()).toEqual([1,0,2,3]);
  });
  it("removed bad values from array that don't match subtype (array)", function() {
    expect(new TypeCop([[1],2,[2]], 'array', 'array').strict()).toEqual([[1],[],[2]]);
  });
});
