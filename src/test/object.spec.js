const TypeCop = require('../../');

const personSchema = {
  first: 'string',
  last: 'string',
  stomach: 'array',
  eyes: {
    color: 'string',
    open: 'boolean',
  },
};

const person = {
  first: 'matt',
  last: 'wski',
  stomach: ['eggs', 'bacon', 'bread'],
  eyes: {
    color: 'green',
    open: true,
  }
};

describe("object():", function() {
  it("returns unmodified object for valid object type with no subtype", function() {
    expect(new TypeCop({imaobject: true}, 'object').strict()).toEqual({imaobject: true});
  });
  it("returns unmodified object for valid object type with a subtype schema", function() {
    expect(new TypeCop(person, 'object', {imaobject: 'boolean'}).strict()).toEqual(person);
  });
});
