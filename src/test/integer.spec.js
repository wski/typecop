const TypeCop = require('../../');

describe("integer():", function() {
  it("returns unmodified number for valid integer type", function() {
    expect(new TypeCop(1234, 'integer').strict()).toBe(1234);
  });

  it("returns a parsed integer if type is string", function() {
    expect(new TypeCop('1234', 'integer').strict()).toBe(1234);
  });

  it("returns a valid number if type is boolean", function() {
    expect(new TypeCop(true, 'integer').strict()).toBe(0);
  });

  it("returns a parsed number if type is array", function() {
    expect(new TypeCop([1234], 'integer').strict()).toBe(1234);
  });

  it("returns a number if type is object", function() {
    expect(new TypeCop({true: true}, 'integer').strict()).toBe(0);
  });

  it("safely creates a number if type is function", function() {
    expect(new TypeCop(function(){}, 'integer').strict()).toBe(0)
  });
});
