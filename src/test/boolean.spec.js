const TypeCop = require('../../');
const tc = new TypeCop();

describe("boolean():", function() {
  it("returns unmodified boolean for valid boolean type", function() {
    expect(tc.strict(true, 'boolean')).toBe(true);
  });

  it("returns a boolean value if type is integer", function() {
    expect(new TypeCop(1234, 'boolean').strict()).toBe(true);
  });

  it("returns a boolean value if type is string", function() {
    expect(new TypeCop('false', 'boolean').strict()).toBe(false);
  });

  it("returns a boolean value array if type is array", function() {
    expect(new TypeCop([true], 'boolean').strict()).toBe(true);
  });

  it("returns a boolean value object if type is object", function() {
    expect(new TypeCop({true: true}, 'boolean').strict()).toBe(true);
  });

  it("safely creates a string if type is function", function() {
    expect(new TypeCop(function(){}, 'boolean').strict()).toBe(true);
  });
});
