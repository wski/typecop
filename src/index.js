import {
  string,
  integer,
  object,
  array,
  boolean,
  validate,
} from './methods';

/**
 * Creates a new TypeCop.
 * @class
 */
class TypeCop {
  /**
  * @constructs TypeCop
  * @param {anything} assignment - thing to validate
  * @param {string} type - type that it should match
  * @param {string} subtype - schema for matching sub types such as array values.
  */
  constructor(assignment, type = 'string', subtype = false, callback) {
    this.callback = callback || function noop() {};
    this.assignment = assignment;
    this.type = type;
    this.subtype = subtype;
  }

  strict(assignment, type, subtype, callback) {
    if (assignment) this.assignment = assignment;
    if (type) this.type = type;
    if (subtype) this.subtype = subtype;
    if (callback) this.callback = callback;

    return validate.call(this);
  }

  string() {
    return string.call(this);
  }

  integer() {
    return integer.call(this);
  }

  object() {
    return object.call(this);
  }

  array() {
    return array.call(this);
  }

  boolean() {
    return boolean.call(this);
  }
}

export default TypeCop;
