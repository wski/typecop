export const boolean = function boolean() {
  let validated = this.assignment;
  // Check to see if the assignment is not a valid string.
  if (typeof this.assignment !== 'boolean') {
    try {
      // the assignment is invalid, let's attempt to make it valid.
      if (this.assignment === 'false') {
        validated = false;
      } else {
        validated = (this.assignment) ? true : false;
      }
      this.callback(
        `TypeCop: converted ${this.assignment} to a boolean of ${validated}.`
      );
    } catch (e) {
      // we were unable to convert the assignment to a proper boolean
      this.callback(
        false,
        `TypeCop: ${this.assignment} was a ${typeof this.assignment} expectet a boolean.`
      );

      // return something valid.
      validated = false;
    }
  }

  return validated;
};
