export const validate = function validate() {
  let validated = this.assignment;

  switch (this.type) {
    case 'string':
      validated = this.string(validated);
      break;

    case 'integer':
      validated = this.integer(validated);
      break;

    case 'object':
      validated = this.object(validated, this.subtype);
      break;

    case 'array':
      validated = this.array(validated, this.subtype);
      break;

    case 'boolean':
      validated = this.boolean(validated);
      break;

    default:
      break;
  }

  return validated;
};
