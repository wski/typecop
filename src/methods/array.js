import TypeCop from '../index.js';

export const array = function array() {
  let index = 0;
  let validated = this.assignment;
  // Check to see if the assignment is not a valid string.

  const validateChildren = () => {
    if (this.subtype) {
      const next = () => {
        index = index + 1;

        validated[index] = new TypeCop(
          validated[index],
          this.subtype,
          null,
          (warning, error) => {
            if (error) {
              this.callback(
                `TypeCop: invalid key ${validated[index]} was found in array, and replaced`
              );
            }

            if (index + 1 < validated.length) {
              next();
            }
          }
        ).strict();
      };

      next();
    }
  };

  if (!Array.isArray(this.assignment)) {
    try {
      // the assignment is invalid, let's attempt to make it valid.
      const temp = JSON.parse(validated);
      // If it worked, let's verify the subtypes.
      if (Array.isArray(temp)) {
        validated = temp;

        validateChildren();
      }
    } catch (e) {
      // we were unable to convert the assignment to a proper array
      this.callback(
        false,
        `TypeCop: ${this.assignment} was a ${typeof this.assignment} expectet an array.`
      );
      // return something valid.
      validated = [];
    }

    if (!Array.isArray(validated)) validated = [];
  } else {
    validateChildren();
  }

  return validated;
};
