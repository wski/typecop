export const integer = function integer() {
  let validated = this.assignment;
  // Check to see if the assignment is not a valid string.
  if (typeof this.assignment !== 'number') {
    try {
      // the assignment is invalid, let's attempt to make it valid.
      validated = parseFloat(this.assignment);
      if (isNaN(validated)) throw new Error();
      this.callback(
        `TypeCop: converted ${this.assignment} to a number of ${validated}.`
      );
    } catch (e) {
      // we were unable to convert the assignment to a proper integer
      this.callback(
        false,
        `TypeCop: ${this.assignment} was a ${typeof this.assignment} expectet a number.`
      );
      // return something valid.
      validated = 0;
    }

    if (!validated) validated = 0;
  }

  return validated;
};
