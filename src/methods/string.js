export const string = function string() {
  let validated = this.assignment;
  // Check to see if the assignment is not a valid string.
  if (typeof this.assignment !== 'string') {
    try {
      // the assignment is invalid, let's attempt to make it valid.
      validated = JSON.stringify(this.assignment);

      this.callback(
        `TypeCop: converted ${this.assignment} to a string of ${validated}.`
      );
    } catch (e) {
      // we were unable to convert the assignment to a proper string
      this.callback(
        false,
        `TypeCop: ${this.assignment} was a ${typeof this.assignment} expectet a string.`
      );

      // return something valid.
      validated = 'string';
    }

    if (!validated) validated = 'string';
  }

  return validated;
};
