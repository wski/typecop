export const object = function object() {
  let validated = this.assignment;
  // Check to see if the assignment is not a valid string.
  let validateChildren = () => {
    // TODO: match children with schema passed to subtype
  };


  if (typeof this.assignment === 'object' && !Array.isArray(this.assignment)) {
    validateChildren();
  } else {
    try {
      // the assignment is invalid, let's attempt to make it valid.
      const temp = JSON.parse(validated);
      // If it worked, let's verify the subtypes.
      if (typeof temp !== 'object' && !Array.isArray(temp)) {
        validated = temp;
        validateChildren();
      }
    } catch (e) {
      // we were unable to convert the assignment to a proper array
      this.callback(
        false,
        `TypeCop: ${this.assignment} was a ${typeof this.assignment} expectet an array.`
      );
      // return something valid.
      validated = {};
    }

    if (typeof this.assignment !== 'object') validated = {};
    if (Array.isArray(this.assignment)) validated = {};
  }

  return validated;
};
