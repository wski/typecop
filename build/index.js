'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var _classCallCheck = _interopDefault(require('babel-runtime/helpers/classCallCheck'));
var _createClass = _interopDefault(require('babel-runtime/helpers/createClass'));
var _typeof = _interopDefault(require('babel-runtime/helpers/typeof'));
var _JSON$stringify = _interopDefault(require('babel-runtime/core-js/json/stringify'));

var validate = function validate() {
  var validated = this.assignment;

  switch (this.type) {
    case 'string':
      validated = this.string(validated);
      break;

    case 'integer':
      validated = this.integer(validated);
      break;

    case 'object':
      validated = this.object(validated, this.subtype);
      break;

    case 'array':
      validated = this.array(validated, this.subtype);
      break;

    case 'boolean':
      validated = this.boolean(validated);
      break;

    default:
      break;
  }

  return validated;
};

var string = function string() {
  var validated = this.assignment;
  // Check to see if the assignment is not a valid string.
  if (typeof this.assignment !== 'string') {
    try {
      // the assignment is invalid, let's attempt to make it valid.
      validated = _JSON$stringify(this.assignment);

      this.callback('TypeCop: converted ' + this.assignment + ' to a string of ' + validated + '.');
    } catch (e) {
      // we were unable to convert the assignment to a proper string
      this.callback(false, 'TypeCop: ' + this.assignment + ' was a ' + _typeof(this.assignment) + ' expectet a string.');

      // return something valid.
      validated = 'string';
    }

    if (!validated) validated = 'string';
  }

  return validated;
};

var integer = function integer() {
  var validated = this.assignment;
  // Check to see if the assignment is not a valid string.
  if (typeof this.assignment !== 'number') {
    try {
      // the assignment is invalid, let's attempt to make it valid.
      validated = parseFloat(this.assignment);
      if (isNaN(validated)) throw new Error();
      this.callback('TypeCop: converted ' + this.assignment + ' to a number of ' + validated + '.');
    } catch (e) {
      // we were unable to convert the assignment to a proper integer
      this.callback(false, 'TypeCop: ' + this.assignment + ' was a ' + _typeof(this.assignment) + ' expectet a number.');
      // return something valid.
      validated = 0;
    }

    if (!validated) validated = 0;
  }

  return validated;
};

var boolean = function boolean() {
  var validated = this.assignment;
  // Check to see if the assignment is not a valid string.
  if (typeof this.assignment !== 'boolean') {
    try {
      // the assignment is invalid, let's attempt to make it valid.
      if (this.assignment === 'false') {
        validated = false;
      } else {
        validated = this.assignment ? true : false;
      }
      this.callback('TypeCop: converted ' + this.assignment + ' to a boolean of ' + validated + '.');
    } catch (e) {
      // we were unable to convert the assignment to a proper boolean
      this.callback(false, 'TypeCop: ' + this.assignment + ' was a ' + _typeof(this.assignment) + ' expectet a boolean.');

      // return something valid.
      validated = false;
    }
  }

  return validated;
};

var array = function array() {
  var _this = this;

  var index = 0;
  var validated = this.assignment;
  // Check to see if the assignment is not a valid string.

  var validateChildren = function validateChildren() {
    if (_this.subtype) {
      (function () {
        var next = function next() {
          index = index + 1;

          validated[index] = new TypeCop(validated[index], _this.subtype, null, function (warning, error) {
            if (error) {
              _this.callback('TypeCop: invalid key ' + validated[index] + ' was found in array, and replaced');
            }

            if (index + 1 < validated.length) {
              next();
            }
          }).strict();
        };

        next();
      })();
    }
  };

  if (!Array.isArray(this.assignment)) {
    try {
      // the assignment is invalid, let's attempt to make it valid.
      var temp = JSON.parse(validated);
      // If it worked, let's verify the subtypes.
      if (Array.isArray(temp)) {
        validated = temp;

        validateChildren();
      }
    } catch (e) {
      // we were unable to convert the assignment to a proper array
      this.callback(false, 'TypeCop: ' + this.assignment + ' was a ' + _typeof(this.assignment) + ' expectet an array.');
      // return something valid.
      validated = [];
    }

    if (!Array.isArray(validated)) validated = [];
  } else {
    validateChildren();
  }

  return validated;
};

var object = function object() {
  var validated = this.assignment;
  // Check to see if the assignment is not a valid string.
  var validateChildren = function validateChildren() {
    // TODO: match children with schema passed to subtype
  };

  if (_typeof(this.assignment) === 'object' && !Array.isArray(this.assignment)) {
    validateChildren();
  } else {
    try {
      // the assignment is invalid, let's attempt to make it valid.
      var temp = JSON.parse(validated);
      // If it worked, let's verify the subtypes.
      if ((typeof temp === 'undefined' ? 'undefined' : _typeof(temp)) !== 'object' && !Array.isArray(temp)) {
        validated = temp;
        validateChildren();
      }
    } catch (e) {
      // we were unable to convert the assignment to a proper array
      this.callback(false, 'TypeCop: ' + this.assignment + ' was a ' + _typeof(this.assignment) + ' expectet an array.');
      // return something valid.
      validated = {};
    }

    if (_typeof(this.assignment) !== 'object') validated = {};
    if (Array.isArray(this.assignment)) validated = {};
  }

  return validated;
};

/**
 * Creates a new TypeCop.
 * @class
 */

var TypeCop = function () {
  /**
  * @constructs TypeCop
  * @param {anything} assignment - thing to validate
  * @param {string} type - type that it should match
  * @param {string} subtype - schema for matching sub types such as array values.
  */

  function TypeCop(assignment) {
    var type = arguments.length <= 1 || arguments[1] === undefined ? 'string' : arguments[1];
    var subtype = arguments.length <= 2 || arguments[2] === undefined ? false : arguments[2];
    var callback = arguments[3];

    _classCallCheck(this, TypeCop);

    this.callback = callback || function noop() {};
    this.assignment = assignment;
    this.type = type;
    this.subtype = subtype;
  }

  _createClass(TypeCop, [{
    key: 'strict',
    value: function strict(assignment, type, subtype, callback) {
      if (assignment) this.assignment = assignment;
      if (type) this.type = type;
      if (subtype) this.subtype = subtype;
      if (callback) this.callback = callback;

      return validate.call(this);
    }
  }, {
    key: 'string',
    value: function string$$() {
      return string.call(this);
    }
  }, {
    key: 'integer',
    value: function integer$$() {
      return integer.call(this);
    }
  }, {
    key: 'object',
    value: function object$$() {
      return object.call(this);
    }
  }, {
    key: 'array',
    value: function array$$() {
      return array.call(this);
    }
  }, {
    key: 'boolean',
    value: function boolean$$() {
      return boolean.call(this);
    }
  }]);

  return TypeCop;
}();

module.exports = TypeCop;